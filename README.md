# Form Validation Problem

[Form Validation Problem from Springload](https://github.com/springload/form-validation-problem)

## Notes
- I spent a lot more than 2 hours on this as it was my first time learning & using React, I was deep in the docs the whole time :)

- My priority was mostly just trying to get the form validation working in a React-y way, and providing feedback to the user (through visual icons & color, error messages and `aria-invalid`). When I've worked with forms in the past, I have relied upon native HTML validation, and used CSS rules like `:focus-within`, `input:focus:valid`, `input:not(:placeholder-shown)` etc to provide visual feedback, so the challenge for me was to rely on React's state as the single source of truth.

- I added images and monkey emoji as I wanted the project to be fun and have some sense of a story and a theme - i.e. why would someone be choosing between particular colours and animals? what kind of website is this? Working with SVG & colour is a very familiar and safe space for me so it was nice to combine it with the new experience of React. (The illustrations are loosely based on [Tiger](https://thenounproject.com/icon/tiger-1080126/), [Bear](https://thenounproject.com/icon/bear-1080106/) and [Donkey](https://thenounproject.com/icon/donkey-1080112/) by Aline Escobar from the Noun Project. She didn't have a snake though...) Note: hacking a font file to replace the bullet points probably isn't best practice and should not be attempted in production environment.

- In terms of accessibility I didn't have time to be particularly thorough, I mostly relied on using native HTML inputs and some basic checks and adjustments to tab order, plus adding a `lang` attribute, and made the focus state obvious to sighted users. There's one accessibility issue which I know that I introduced, and didn't find a good fix for: using `display: contents` visually hides the ugly and awkward native `fieldset`, but it also hides it from the accessibility tree in Safari (for now, anyway - fixed this month in [TP 144](https://trac.webkit.org/changeset/291570/webkit/)).

## Challenges

- What I struggled with (as you'll probably realise from looking at the code) was the **structure** of the project. Although I've read a lot of React _docs_ in the last week, I haven't yet explored many other people's React _projects_, so I learned a bunch about how to do things right on a granular level, but didn't learn much about _'where to put things'_.  

- Mostly React made a lot of sense coming from Vue & Svelte, there seem to be a lot of transferable concepts. I haven't quite got to grips with how best to work with styling in JSX though, there seem to be quite a few competing approaches. So I kept it very basic for now, with CSS in a single file, plus a few inline style rules (where the style was linked to the state). 

- I had a few little battles with Typescript but I'll chalk them up as 'character-building'. There was one where I admitted defeat though: `ref={summary}` on line 38 of `ErrorSummary.tsx`

## What I didn't do
- Browser support: I didn't try to support older browsers at all. This would likely be pretty disastrous in IE, as I've made heavy use of CSS variables for colors and layout, without any fallbacks...😬
One example of a (non-critical) feature I've used with little browser support is the `accent-color` CSS property for radio button colours - on my machine this works on Firefox but both Chromium and GNOME Web (Webkit) show the standard native styling.

- I didn't create a 'form submitted' page or even disable the inputs after successfully submitting, I only added a little success message to show that it worked... so that part of the experience certainly doesn't feel real.
- I didn't include any tests.
- I didn't create a proper responsive layout - this design isn't optimised for either desktop or mobile, to be honest I was aiming for 'not broken' on both. 
- I haven't written any documentation yet, beyond a meagre sprinkling of comments.
- Normally I would use a bunch of transitions & animations, eg. to switch between the animals and background colours, animate components in and out of the DOM, or indicate that a form is being submitted etc. No time for that on this occasion!
