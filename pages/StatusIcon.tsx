function TickIcon () {
  return (
  <svg className="icon-status" viewBox="0 0 100 100"
    fill="none" stroke="var(--color-success)" aria-hidden="true"
    strokeWidth="10" strokeLinecap="round" strokeLinejoin="round">
    <circle r="50" cx="50" cy="50"/>
    <path d="m23,58 20,16 30,-45"/>
  </svg>)
}

function CrossIcon () {
  return (
  <svg className="icon-status" viewBox="0 0 100 100"
    fill="none" stroke="var(--color-warning)" aria-hidden="true"
    strokeWidth="10" strokeLinecap="round" strokeLinejoin="round">
    <circle r="50" cx="50" cy="50"/>
    <path d="M28,28l44,44m-44,0l44,-44"/>
  </svg>)
}
function EmptyIcon () {
  return (<svg className="icon-status" viewBox="0 0 100 100" aria-hidden="true"></svg>)
}

export function StatusIcon ({
  isValid, 
  hasEnteredText, 
  notInFocus
}: {
  isValid: boolean, 
  hasEnteredText: boolean, 
  notInFocus: boolean 
}) {
  if (isValid) {
    return (<TickIcon/>) 
  } else if (notInFocus) {
    // Error icon should not show while typing
    return (hasEnteredText) ? (<CrossIcon/>)
                            : (<EmptyIcon/>)
  } else {
    return (<EmptyIcon/>)
  }
}