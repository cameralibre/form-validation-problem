function focusOrErrorColor (
  focused: string, 
  showError: Boolean, 
  inputId: string
) {
  if (focused === inputId) {
    return 'var(--color-focus)'
  } else {
    return (showError) ? 'var(--color-warning)' : 'currentColor'
  }                                                  
}

const animalList: string[] = ['bear', 'tiger', 'snake', 'donkey']
const colourList: string[] = ['blue', 'green', 'red', 'black', 'brown']
const size = '1em'

export function EmailIcon ({ 
  showError = false, 
  focused 
}: {
  showError: Boolean, 
  focused: string
}) {
  const strokeColor = focusOrErrorColor(focused, showError, 'input-email')
  return (
    <svg width={size} height={size} className="icon-label" viewBox="0 0 100 100" aria-hidden
    strokeWidth="7" fill="currentColor" fillOpacity="0.2" stroke={strokeColor} strokeLinejoin="round">
      <path d="m11 36 30 30c6 6 12 6 18 0l30-30m-6-3h-66c-9 0-9 0-9 9v42c0 9 0 9 9 9h66c9 0 9 0 9-9v-42c0-9 0-9-9-9z"/>
    </svg>
  )
}
export function PasswordIcon ({ 
  showError = false, 
  focused 
}: {
  showError: Boolean, 
  focused: string
}) {
  const strokeColor = focusOrErrorColor(focused, showError, 'input-password')
  return (
    <svg width={size} height={size} className="icon-label" viewBox="0 0 100 100" aria-hidden
    strokeWidth="7" fill="currentColor" fillOpacity="0.2" stroke={strokeColor} strokeLinejoin="round">
      <path d="m72.1 37.4h-43.6c-11.9 0-11.9 0-11.9 11.9v31.7c0 11.9 0 11.9 11.9 11.9h43.6c11.9 0 11.9 0 11.9-11.9v-31.7c0-11.9 0-11.9-11.9-11.9z"/>
      <path fill="none" d="m26.6 33.6v-3.9c0-8 3.9-23.7 24-23.7 19.6 0 23.7 15.7 23.7 23.7v3.9"/>
    </svg>
  )
}

export function AnimalIcon ({ focused }: { focused: string }) {
  const strokeColor = (animalList.includes(focused)) ? 'var(--color-focus)' 
  : 'currentColor'
  return (
    <svg width={size} height={size} className="icon-label" viewBox="0 0 100 100" aria-hidden
    strokeWidth="7" fill="currentColor" fillOpacity="0.2" stroke={strokeColor} >
      <path d="m93 62a15.5 15.5 90 00-18.5-14.1c-2.5.5-5.3 0-7.3-1.7a17.3 17.3 90 00-27.1 15.4c.1 2.6-.5 4.9-2.1 6.9a15.5 15.5 90 00-3.6 9.8c0 4.7 1.9 9 5.3 12a13.7 13.7 90 0011 3.4c7.3-.7 9.4-4.8 11.4-8.2 1.3-2 2.3-3.9 4.2-5s7.1-2.2 11.2-2.2a15.5 15.5 90 0015.5-16.3zm-46.3-33.6c2.2 2.6 4.6 3.8 6.9 3.8l.9-.1c5.4-1.1 7.8-11 6.6-16.8-.6-2.9-2.1-5.2-4.2-6.8a9.3 9.3 90 00-7.4-1.7c-2.5.6-4.6 2.2-6 4.6-1.3 2.3-1.8 5.2-1.3 7.9.7 2.8 2.4 6.3 4.5 9.1zm-15.2-3.7c-4-4.3-10.8-5.2-14.6-1.3-4 3.7-3.9 10.1.1 14.6 1.9 2.2 5.3 4.3 8.5 5.4 1.6.4 3.3.8 4.6.8 1.7 0 3.2-.4 4.3-1.3 4.1-3.7 1.3-13.8-2.9-18.1zm-11.3 26.4c-5.2-2.7-10.8-1.5-13.1 2.3-2.3 4-.6 9.3 3.8 11.8 2.4 1.3 5.5 2.2 8.4 2.4h.4c3.3 0 5.5-1.1 6.7-2.9 2.2-4-2.6-11.4-6.2-13.6zm56.7-15.1h0c2.5 0 4.4-.5 6.5-2.5 1.9-2.3 3.6-5.1 4.2-7.6.4-2.2.1-4.8-.9-6.8-1.1-2.1-2.9-3.6-5.1-4-2.2-.6-4.3-.1-6.3 1.3s-3.4 3.4-4 5.7-.5 5.6.3 8.5c.8 3.4 2.4 5.4 5.3 5.4z"/>
    </svg>
  )
}

export function ColourIcon ({ focused }: { focused: string }) {
  const strokeColor = (colourList.includes(focused)) ? 'var(--color-focus)' 
  : 'currentColor'
  return (
    <svg width={size} height={size} className="icon-label" viewBox="0 0 100 100" aria-hidden
    strokeWidth="7" strokeLinecap="round" stroke={strokeColor} >
    <path fill="currentColor" fillOpacity="0.2" 
      d="m81.5 69h-20.5c-5.8-.1-10.5 4.6-10.5 10.7v1.8c0 7.4-7.1 12.6-14 10.3-19.3-6.6-31.2-26.6-27.7-47.5 3.1-20.6 20.8-35.9 41.1-35.9 23-.3 42 18.7 42.5 42.5 0 3.4-.3 6.6-1 10.1-1 4.8-5 7.9-9.9 8z"/>
    <defs>
      <path fill="none" id="paint" d="M-.5,0 L.5,0"/>
    </defs>
    <use href="#paint" x="49.5" y="29.4"/>
    <use href="#paint" x="32.1" y="39.6"/>
    <use href="#paint" x="32.1" y="59.8"/>
    <use href="#paint" x="67" y="39.6"/>
  </svg>
  )
}

