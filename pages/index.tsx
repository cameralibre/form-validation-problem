import type { NextPage } from 'next'
import Head from 'next/head'
import { Animal, AnimalName, animalList } from './animals'
import { colours, ColourName, colourList } from './colours'
import { StatusIcon } from './StatusIcon'
import { RadioButton } from './RadioButton'
import { ErrorSummary } from './ErrorSummary'
import { SubmitButton } from './SubmitButton'
import { EmailIcon, PasswordIcon, ColourIcon, AnimalIcon } from './LabelIcons'
import { SuccessMessage } from './SuccessMessage'
import { useState, ChangeEvent, FormEvent } from 'react'

type InputId = ('input-email' | 'input-password' | 'input-tiger-type' | '')

const Form: NextPage = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [tigerType, setTigerType] = useState('')
  const [animal, setAnimal] = useState<AnimalName>('none')
  const [colour, setColour] = useState<ColourName>('none')
  const [focused, setFocused] = useState<InputId | AnimalName | ColourName>('')
  const [submitted, setSubmitted] = useState(false)
  const [success, setSuccess] = useState(false)


  // Validation checks
  const isEmailValid = ()  => {
      const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return regex.test(email.toLowerCase())
  }
  const isPasswordValid = () => Boolean(password.length >= 8)
  const isTigerTypeValid = () => (animal === 'tiger') ? Boolean(tigerType.length > 0) : true
  const hasEnteredText = (str: string) => Boolean(str.length)
  const allValid = () => (isEmailValid() && isPasswordValid()) ? isTigerTypeValid() : false
  const showError = (inputName: string) => {
    return (submitted) ? Boolean( (inputName === 'email' && !isEmailValid()) 
                               || (inputName === 'password' && !isPasswordValid())
                               || (inputName === 'tiger-type' && !isTigerTypeValid()) )
                       : false
  }


  // Event handlers
  function handleOptionChange (e: ChangeEvent<HTMLInputElement>) {
    if (colourList.includes(e.currentTarget.value as ColourName)) {
      setColour(e.currentTarget.value as ColourName)
    } else if (animalList.includes(e.currentTarget.value as AnimalName)) {
      setAnimal(e.currentTarget.value as AnimalName)
    }
  }

  function handleSubmit (e: FormEvent) {
    e.preventDefault()
    setSubmitted(true)
    setSuccess(allValid())
  }



  return (
    <>
      <Head>
        <title>Contact Form</title>
        <meta name="description" content="Get in touch" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header>
        <h1>Contact Form</h1>
      </header>
      <form 
        onSubmit={handleSubmit}
        autoComplete='on'
        noValidate>

        <div 
          className='section-textinput'
          style={{ 
            backgroundColor: showError('email') ? 'var(--color-error-background)' 
                                                : 'transparent',
            paddingBottom: showError('email') ? '0' 
                                              : '2.6em'
          }}>
          <label 
            htmlFor='input-email'
            className='label-section'>
            <EmailIcon focused={focused} showError={showError('email')}/>
            <span>Email: </span><span className="required">*</span> 
          </label>
          <div className='input-plus-status'>
            <input 
              id='input-email'
              className='input-text'
              value={email} 
              placeholder='name@example.com'
              onChange={(e) => setEmail(e.target.value)}
              onFocus={(e) => setFocused('input-email')}
              onBlur={(e) => setFocused('')}
              aria-invalid={!isEmailValid()}
              autoComplete='email'
              type='email' 
              name='email' 
              required
            />
            <StatusIcon 
              isValid={isEmailValid()} 
              notInFocus={(focused !== 'input-email')}
              hasEnteredText={(hasEnteredText(email))}
            />
          </div>  
          { (showError('email')) && (
            <p className='error'>Email must be a valid email address.</p>
          )}
        </div>

        <div 
          className='section-textinput' style={{ 
            backgroundColor: showError('password') ? 'var(--color-error-background)' 
                                                   : 'transparent',
            paddingBottom: showError('password') ? '0' 
                                                 : '2.6em'
          }}>
          <label 
            htmlFor='input-password'
            className='label-section'>
            <PasswordIcon focused={focused} showError={showError('password')}/>
            <span>Password: </span><span className="required">*</span> 
          </label>
          <div className='input-plus-status'>
            <input 
              id='input-password'
              className='input-text'
              value={password} 
              placeholder='Enter Password'
              onChange={(e) => setPassword(e.target.value)}
              onFocus={(e) => setFocused('input-password')}
              onBlur={(e) => setFocused('')}
              aria-invalid={!isPasswordValid()}
              autoComplete='new-password'
              type='password' 
              name='password' 
              required
              />
            <StatusIcon 
              isValid={isPasswordValid()} 
              notInFocus={(focused !== 'input-password')}
              hasEnteredText={(hasEnteredText(password))}
              />        
          </div>
          { (showError('password')) && (
            <p className='error'>Password must be at least 8 characters.</p>
          )}
        </div>


        <fieldset>
          <div className='fieldset-contents'>
          <legend className='label-section'>
            <ColourIcon focused={focused}/><span>Colour:</span>
          </legend>
            <div className='buttons-radio'>
              <RadioButton 
                option='blue'  
                group="colour" 
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('blue')}
                onBlur={(e) => setFocused('')}
              />
              <RadioButton 
                option='green' 
                group="colour" 
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('green')}
                onBlur={(e) => setFocused('')}
              />
              <RadioButton 
                option='red'   
                group="colour" 
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('red')}
                onBlur={(e) => setFocused('')}
              />
              <RadioButton 
                option='black' 
                group="colour" 
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('black')}
                onBlur={(e) => setFocused('')}
              />
              <RadioButton 
                option='brown' 
                group="colour" 
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('brown')}
                onBlur={(e) => setFocused('')}
              />
            </div>
          </div>
        </fieldset>

        <fieldset>
          <div className='fieldset-contents'>
          <legend className='label-section'>
          <AnimalIcon focused={focused}/><span>Animals:</span>
          </legend>
            <div className='buttons-radio'>
              <RadioButton 
                option='bear'   
                group="animal"
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('bear')}
                onBlur={(e) => setFocused('')}/>
              <RadioButton 
                option='tiger'  
                group="animal"
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('tiger')}
                onBlur={(e) => setFocused('')}/>
              <RadioButton 
                option='snake'  
                group="animal"
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('snake')}
                onBlur={(e) => setFocused('')}/>
              <RadioButton 
                option='donkey' 
                group="animal"
                colour={colour} 
                animal={animal}
                onChange={handleOptionChange}
                onFocus={(e) => setFocused('donkey')}
                onBlur={(e) => setFocused('')}/>
            </div>
          </div>
        </fieldset>
        
        <Animal 
          animalName={animal}
          colour={colour}
          colours={colours}
        />

        { (animal === 'tiger') && (
          <div 
            className='section-textinput'
            style={{ 
              backgroundColor: showError('tiger-type') ? 'var(--color-error-background)' 
                                                      : 'transparent',
              paddingBottom: showError('tiger-type') ? '0' 
                                                    : '2.6em'
            }}>
            <label 
              htmlFor='input-tiger-type'
              className='label-section'>
              Type of tiger: <span className="required">*</span> 
            </label>
            <div className='input-plus-status'>
              <input 
                id='input-tiger-type'
                className='input-text'
                value={tigerType} 
                placeholder='Enter description or classification'
                onChange={(e) => setTigerType(e.target.value)}
                autoComplete='off'
                type='text' 
                name='tiger-type' 
                aria-invalid={!isTigerTypeValid()}
                required
              />
              <StatusIcon 
                isValid={(hasEnteredText(tigerType))} 
                notInFocus={(focused !== 'input-tiger-type')}
                hasEnteredText={(hasEnteredText(tigerType))}
              /> 
            </div> 
            { (showError('tiger-type')) && (
              <p className='error'>Provide a description (e.g. &lsquo;fierce&rsquo;) or a classification (e.g. &lsquo;Bengal&rsquo;).</p>
            )}  
          </div>)
        }
        <div 
          className='section-submit'
          style={{
            borderColor: (submitted && !allValid()) ? 'var(--color-warning)' 
                                                    : 'transparent'
          }}>
          {(submitted && !allValid()) && (
            <ErrorSummary 
              emailValid={isEmailValid()}
              passwordValid={isPasswordValid()}
              tigerTypeValid={isTigerTypeValid()}
            />
          )}
          {(success && allValid()) && (
            <SuccessMessage/>
          )}
          <SubmitButton 
            allValid={allValid()}
            submitted={submitted}
          />
        </div>
        
      </form>
    </>

  )
}

export default Form
