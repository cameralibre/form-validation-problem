  export function SuccessMessage () {
    return (
      <div className='success-message'>
        <h2>Thanks!</h2>
        <p>We'll be in touch</p>
      </div>
    )
  }