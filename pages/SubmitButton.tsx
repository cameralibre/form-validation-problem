
  export function SubmitButton ({ allValid, submitted }: { allValid: boolean, submitted: boolean }) {
    return (
      <button 
        className='button-submit'
        type='submit'
        style={{
          backgroundColor: (allValid) ? 'var(--color-success)' : 'transparent',
          color:           (allValid) ? 'white' : 'var(--primary)',
          borderColor:     (allValid) ? 'var(--color-success)' 
                                      : (submitted) ? 'var(--color-warning)' : 'var(--primary)',
          borderWidth:     (allValid) ? '2px' 
                                      : (submitted) ? '2px' : '1px',
          fontWeight:      (allValid) ? '600' : '400'
        }}
      > 
        Submit
      </button>
    )
  }