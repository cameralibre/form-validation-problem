
export type ColourName = ('blue' | 'green' | 'red' | 'black' | 'brown' | 'none')
export interface Colours {
  [key: string]: ColourOptions
}
export interface ColourOptions {
  blue: string,
  green: string,
  red: string,
  black: string,
  brown: string,
  none: string
}


export const colourList: ColourName[] = ['blue', 'green', 'red', 'black', 'brown']

export const colours: Colours = {
  background: {
    blue:  '#227c8c',
    green: '#2d9551',
    red:   '#d64356',
    black: '#242826',
    brown: '#6a482f',
    none:  '#ffffff'
  },
  bearPrimary: {
    blue:  '#835e47',
    green: '#995248',
    red:   '#bba95b',
    black: '#d1b235',
    brown: '#a19264',
    none:  '#777777'
  },
  bearOutline: {
    blue:  '#453023',
    green: '#39211e',
    red:   '#39331d',
    black: '#332d20',
    brown: '#3f3a2d',
    none:  '#333333'
  },
  bearLight: {
    blue:  '#b18f7d',
    green: '#b27770',
    red:   '#e1cf8d',
    black: '#d3bb74',
    brown: '#d7c58e',
    none:  '#bebebe'
  },
  bearDark: {
    blue:  '#382e2a',
    green: '#3e3333',
    red:   '#39331d',
    black: '#36322a',
    brown: '#35332c',
    none:  '#222222'
  },
  bearEyes: {
    blue:  '#e0f0f5',
    green: '#ebf3ed',
    red:   '#f8f1f2',
    black: '#f7f7f0',
    brown: '#f5efee',
    none:  '#eaeaea'
  },
  tigerPrimary: {
    blue:  '#e18c00',
    green: '#f77c36',
    red:   '#dfa000',
    black: '#ff743d',
    brown: '#dcb53a',
    none:  '#838383'
  },
  tigerOutline: {
    blue:  '#4d3319',
    green: '#513023',
    red:   '#4e3500',
    black: '#71341f',
    brown: '#40340f',
    none:  '#4d4d4d'
  },
  tigerDark: {
    blue:  '#352417',
    green: '#3e2319',
    red:   '#312616',
    black: '#411e14',
    brown: '#2e2717',
    none:  '#282828'
  },
  tigerLight: {
    blue:  '#fcf2ec',
    green: '#fdf2ef',
    red:   '#fff1e4',
    black: '#fcf2f0',
    brown: '#fcf3e2',
    none:  '#f4f4f4'
  },
  snakePrimary: {
    blue:  '#cbba4b',
    green: '#edab31',
    red:   '#b4c032',
    black: '#249e5b',
    brown: '#34cf9f',
    none:  '#979797'
  },
  snakeLight: {
    blue:  '#fbeca8',
    green: '#fde3c8',
    red:   '#ddeb63',
    black: '#75e9a0',
    brown: '#8af7ce',
    none:  '#eeeeee'
  },
  snakeDark: {
    blue:  '#605936',
    green: '#876638',
    red:   '#757d38',
    black: '#244b32',
    brown: '#197458',
    none:  '#4b4b4b'
  },
  snakeOutline: {
    blue:  '#423f33',
    green: '#53463c',
    red:   '#494c2c',
    black: '#02180f',
    brown: '#263d34',
    none:  '#3f3f3f'
  },
  snakeTongue: {
    blue:  '#e86f00',
    green: '#6e8fdf',
    red:   '#a975ff',
    black: '#d58d5a',
    brown: '#e98383',
    none:  '#e9e9e9'
  },
  donkeyPrimary: {
    blue:  '#8c755e',
    green: '#b48fa4',
    red:   '#c79f4d',
    black: '#c58e51',
    brown: '#9e9a83',
    none:  '#999999'
  },
  donkeyLight: {
    blue:  '#d7cabf',
    green: '#d9c8d3',
    red:   '#f2d19d',
    black: '#d6c1b1',
    brown: '#d7d4c5',
    none:  '#dfdfdf'
  },
  donkeyOutline: {
    blue:  '#302823',
    green: '#382230',
    red:   '#473c2c',
    black: '#211811',
    brown: '#2f2e28',
    none:  '#3c3c3c'
  },
  donkeyEye: {
    blue:  '#e6dfda',
    green: '#e8dde3',
    red:   '#f3ede5',
    black: '#e7e1dd',
    brown: '#f0eee3',
    none:  '#eaeaea'
  }
}
