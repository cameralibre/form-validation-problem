import { useRef, useEffect } from "react"

export function ErrorSummary ({ 
  emailValid, 
  passwordValid, 
  tigerTypeValid }: { 
    emailValid: boolean, 
    passwordValid: boolean, 
    tigerTypeValid: boolean
  }) {
  const summary = useRef<HTMLHeadingElement>()

  function ErrorMessage () {
    const AnEmail = () => (!emailValid) ? (<a href="#input-email">an email</a>) : null
    const APassword = () => (!passwordValid) ? (<a href="#input-password">a password</a>): null
    const ATypeOfTiger = () => (!tigerTypeValid) ? (<a href="#input-tiger-type">a type of tiger</a>) : null
    
    const missingFields = [AnEmail(), APassword(), ATypeOfTiger()].filter(item => item !== null)
    const total = missingFields.length
    return (
      <p className='error'>
         <span>Please enter </span>
         {(total === 3) && missingFields[total - 3]}{(total === 3) && (<span>, </span>)}
         {(total >= 2) && missingFields[total - 2]}{(total >= 2) &&  (<span> and </span>)} 
         {missingFields[total - 1]}<span>.</span> 
      </p>
    )
  }

  useEffect(()=>{
    if (summary.current !== undefined) {
      summary.current.focus()
    }
  }, [])

  return (
    <div className='error-summary'>
      <h2 ref={summary}>Missing Fields</h2>
      <ErrorMessage/>
    </div>
  )
}