import { ChangeEventHandler, FocusEventHandler } from "react"
import { ColourName, colours } from "./colours"
import { AnimalName } from "./animals"


export function RadioButton ({ 
  option, 
  group, 
  colour, 
  animal, 
  onChange, 
  onFocus, 
  onBlur 
}: { 
  option: string, 
  group: string, 
  colour: ColourName, 
  animal: AnimalName, 
  onChange: ChangeEventHandler, 
  onFocus: FocusEventHandler, 
  onBlur: FocusEventHandler
}) {
  const checked = Boolean((colour === option) || (animal === option))
  return (
  <>
    <label 
      htmlFor={option}
      className="label-radio"
      style={{
        fontWeight: (checked) ? 600 : 400
      }}
    >
    <input 
      id={option}
      value={option}
      type="radio" 
      name={group} 
      checked={checked}
      onChange={onChange}
      onFocus={onFocus}
      onBlur={onBlur}
      style={{ 
        accentColor: colours.background[colour]
      }}
      tabIndex={0}
    />
      <span>{option}</span>
    </label>
  </>
  )
}