import { ColourName, Colours } from "./colours"

export type AnimalName = ('bear' | 'donkey' | 'snake' | 'tiger' | 'none')
export const animalList: AnimalName[] = ['bear', 'donkey', 'snake', 'tiger']

export function Animal ({ animalName, colour = 'none', colours }: { animalName: AnimalName, colour: ColourName, colours: Colours }) {
  const bearColours = {
    '--animal-background': colours.background[colour],
    '--bear-primary': colours.bearPrimary[colour],
    '--bear-outline': colours.bearOutline[colour],
    '--bear-light': colours.bearLight[colour],
    '--bear-dark': colours.bearDark[colour],
    '--bear-eyes': colours.bearEyes[colour]
  }
  const donkeyColours = {
    '--animal-background': colours.background[colour],
    '--donkey-primary': colours.donkeyPrimary[colour],
    '--donkey-light': colours.donkeyLight[colour],
    '--donkey-outline': colours.donkeyOutline[colour],
    '--donkey-eye': colours.donkeyEye[colour]
  }
  const snakeColours = {
    '--animal-background': colours.background[colour],
    '--snake-primary': colours.snakePrimary[colour],
    '--snake-light': colours.snakeLight[colour],
    '--snake-dark': colours.snakeDark[colour],
    '--snake-outline': colours.snakeOutline[colour],
    '--snake-tongue': colours.snakeTongue[colour],
  }
  const tigerColours = {
    '--animal-background': colours.background[colour],
    '--tiger-primary': colours.tigerPrimary[colour],
    '--tiger-outline': colours.tigerOutline[colour],
    '--tiger-dark': colours.tigerDark[colour],
    '--tiger-light': colours.tigerLight[colour]
  }
  const backgroundOnly = {
    '--animal-background': colours.background[colour]
  }

  switch(animalName) {
    case 'bear':
      return (<Bear style={ bearColours }/>)
    case 'donkey':
      return <Donkey style={ donkeyColours }/>
    case 'snake':
      return <Snake style={ snakeColours }/>
    case 'tiger':
      return <Tiger style={ tigerColours }/>
    default:
      return <EmptyImage style={ backgroundOnly }/>
  }
}

function EmptyImage ({ style }: { style: Record<string, string> }) {
return (
  <svg viewBox='0 0 100 100' className='animal' style={style}>
    <rect width="100" height="100" fill='var(--animal-background)'/>
  </svg>
)}

function Bear ({ style }: { style: Record<string, string> }) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" 
      viewBox="0 -2 100 100" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3"
      className="animal" style={style}

    >
      <g id="bear-l-ear">
        <path fill="var(--bear-primary)" d="M16.7 31.2c-1.7-1-3-2.2-4-3.9C9 21.6 11 13.8 17.2 10c3.2-2 6.8-2.6 9.9-2 3.2.6 4.9 5.7 6.7 8.6"/>
        <path fill="var(--bear-light)" d="M16.7 31.2c-3.3-1-7-7-5.3-13.6 4.5-.2 9.2 2.4 11.7 6"/>
        <path fill="none" stroke="var(--bear-outline)" d="M23 23.7s-3.7-6.2-11.6-6"/>
        <path fill="none" stroke="var(--bear-outline)" d="M16.7 31.2c-1.7-1-3-2.2-4-3.9C9 21.6 11 13.8 17.2 10c3.2-2 6.8-2.6 9.9-2 3.2.6 4.9 5.7 6.7 8.6"/>
      </g>
      <use href="#bear-l-ear" 
        transform-origin="50px 0px 0px" transform="scale(-1, 1)" />
      <circle id="bear-face" 
        cx="50" cy="54" r="39" fill="var(--bear-primary)" stroke="var(--bear-outline)"/>
      <g id="bear-eyes" 
        fill="var(--bear-eyes)">
        <ellipse cx="28.9" cy="47" rx="6.6" ry="8.1" />
        <ellipse cx="70.9" cy="47" rx="6.6" ry="8.1" />
      </g>
      <g id="bear-pupils" 
        fill="var(--bear-dark)">
        <circle cx="31" cy="48.7" r="2.5"/>
        <circle cx="73" cy="48.7" r="2.5" />
      </g>
      <circle id="bear-snout" 
        cx="50" cy="72.5" r="19.9" fill="var(--bear-light)" stroke="var(--bear-outline)"/>
      <path id="bear-mouth" 
        fill="none" stroke="var(--bear-dark)" d="m40 77.7c3.7 6.1 9.9 2.2 9.9-2v-5.9v5.9 c0 4.2 6.2 8.1 9.9 2"/>
      <path id="bear-nose" 
        fill="var(--bear-dark)" d="M57.2 66.7c0 3-4.5 5.5-7.3 5.6-2.7 0-7.2-2.6-7.2-5.6 0-3.1-1.2-5.6 7.2-5.6 8.5-.1 7.3 2.5 7.3 5.6z"/>
    </svg>
  )
} 

function Donkey ({ style }: { style: Record<string, string> }) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 5 100 100"
      strokeLinecap="round" strokeLinejoin="round" strokeWidth="3"
      className="animal" style={style}
      >
      <title>Donkey</title>
      <path id="donkey-neck-fill"
            fill="var(--donkey-primary)"
            d="m31.6 94 2-22.2h32.8l2 22.2c-10 6-26.8 6-36.8 0z"/>
      <path id="donkey-neck"
            fill="none" stroke="var(--donkey-outline)"
            d="M32 94 35 72H65L68 94"/>
      <g id="donkey-l-ear"
        stroke="var(--donkey-outline)">
        <path fill="var(--donkey-light)" d="M44.5 48.3c0 0-16.5-8.5-20.6-18.6-3.4-8.6-3.5-12.6-2.5-15.6"/>
        <path fill="var(--donkey-primary)" d="m44.5 48.3-7.3-20.2c-2.4-6.6-12.9-16-15.7-14.7-.6.5-.6 2.4-.6 4.7 7.9 4.2 9.5 7.2 23.6 30.2"/>
      </g>
      <use href="#donkey-l-ear" transform-origin="50px 48.3px 0px" transform="scale(-1,1)" />
      <path id="donkey-face"
        fill="var(--donkey-primary)" stroke="none"
            d="M39 81 30 71C28 69 29 59 30 54 32 43 37 37 44 34 47 33 53 33 56 34 63 37 68 43 70 54 71 59 72 69 70 71L61 81"/>
      <g id="donkey-snout"
        fill="var(--donkey-light)" stroke="var(--donkey-outline)">
        <path d="M58 87C59 92 55 95 51 95H49C45 95 41 92 42 87"/>
        <ellipse cx="50" cy="80" rx="9.8" ry="10.2" />
      </g>
      <g fill="var(--donkey-outline)">
        <circle cx="54" cy="83" r="1.5"/>
        <circle cx="46" cy="83" r="1.5"/>
      </g>
      <g fill="var(--donkey-eye)">
        <ellipse transform="rotate(10)" transform-origin="65.1px 59.7px 0px" cx="65.1" cy="59.7" rx="5" ry="8"/>
        <ellipse transform="rotate(-10)" transform-origin="34.9px 59.7px 0px" cx="34.9" cy="59.7" rx="5" ry="8"/>
      </g>
      <g fill="var(--donkey-outline)">
        <circle cx="64.1" cy="60.7" r="2.5"/>
        <circle cx="35.9" cy="60.7" r="2.5"/>
      </g>
      <path id="donkey-face"
        fill="none" stroke="var(--donkey-outline)"
            d="M39 81 30 71C28 69 29 59 30 54 32 43 37 37 44 34 47 33 53 33 56 34 63 37 68 43 70 54 71 59 72 69 70 71L61 81"/>
    </svg>
  )
}

function Snake ({ style }: { style: Record<string, string> }) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg"
        viewBox="0 5 100 100"
        strokeLinecap="round" strokeLinejoin="round" strokeWidth="3"
        className="animal" style={style}
        >
      <defs>
        <path id="snake-hood"
          d="m68 92c1.2-32.2 21.5-37.8 23.9-50.8 3.1-17.8-18.9-25.2-31.9-23.4-5 .5-7.7.9-10.2.9-2.5 0-4.8-.4-9.8-.9-13-1.8-35.1 5.6-31.9 23.4 2.4 13 22.7 18.6 23.9 50.8"/>

        <path id="snake-head-shape"
          d="m30 43c-1.7-1.4-6-4-7-6-2-5-3-16 3-18 8-2 17 0 24 0 7 0 16-2 24 0 6 2 5 13 3 18-1 2-5.3 4.6-7 6-4 3-8 9-20 9-12 0-16-6-20-9z"/>
      </defs>

      <use href="#snake-hood" fill="var(--snake-dark)"/>
      <g id="snake-body">
        <path fill="none" stroke="var(--snake-light)"
              d="M61.8 90.5c-.5-7-1.9-24.6-1-35.5"/>
        <path fill="none" stroke="var(--snake-light)"
              d="M38.2 90.5c.5-7 1.9-24.6 1-35.5"/>
        <path id="snake-neck"
          fill="var(--snake-primary)" stroke="var(--snake-outline)"
              d="M68 92c-1.1-12-8-38.2 1-48.2l-38 .6c9 10 2 35.6 1 47.6 15 8 21 8.2 36 0z"/>
        <path id="snake-underbelly"
          fill="none" stroke="var(--snake-light)" d="M56.5 59a16.3 16.3 0 0 1-13 0"/>
        <use href="#snake-underbelly" y="9"/>
        <use href="#snake-underbelly" y="19"/>
        <use href="#snake-underbelly" y="30"/>
      </g>

      <g id="snake-head">
        <use href="#snake-head-shape" fill="var(--snake-primary)" />
        <path id="snake-head-top"
          fill="var(--snake-light)" d="M77.5 20.5c-3.2-3.2-18.5-1.8-27.3-1.8-8.7 0-25.2-1.2-28.4 2 3.3-.2-1.8 5 0 3.1 9.2-10.3 21.8 9.4 28.4 9.4 7 0 18.6-20.5 27.2-9.4 1.4 1.7-2.6-3.6 0-3.3z"/>
        <path id="snake-mouth"
          fill="none" stroke="var(--snake-outline)" d="M 61 41 C 58 45 52 46 50 46 C 48 46 42 45 39 41"/>
        <path id="snake-nostril-l" fill="var(--snake-outline)"
              d="m42.3 33.9c-.6 0-.8.5-.9.8-.2.8 1.1 2.3 2.1 2.7 1 .3 2.1-1 1.3-2.2-.8-1.2-1.8-1.3-2.5-1.3z"/>
        <use href="#snake-nostril-l" transform-origin="50px 50px 0px" transform="scale(-1,1)"/>       
        <g id="snake-eye-l">
          <ellipse fill="var(--snake-light)" transform="rotate(25)" transform-origin="31px 26.3px 0px" cx="31" cy="26.3" rx="6" ry="3"/>
          <circle fill="var(--snake-outline)" cx="31" cy="26.3" r="2.5"/>
        </g>
        <use href="#snake-eye-l" transform-origin="50px 50px 0px" transform="scale(-1,1)"/>

        <use href="#snake-head-shape" fill="none" stroke="var(--snake-outline)" />
        <path fill="var(--snake-tongue)" d="M51.5 50.4c1.3.6 2.6.5 2.6.5.8 0 .8-.4.8-.4-1.6-1.4-3.4-1.7-3.4-4.5a1.5 1.5 0 0 0-3 0c0 2.8-1.8 3.1-3.4 4.5 0 0 0 .4.8.4 0 0 1.3 0 2.6-.5.5-.2 1-.7 1.5-1.1.5.4 1 .9 1.5 1.1z"/>
      </g>
      <use href="#snake-hood" fill="none" stroke="var(--snake-outline)"/>
    </svg>

  )
} 

function Tiger ({ style }: { style: Record<string, string> }) {
return (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
    strokeLinecap="round" strokeLinejoin="round" strokeWidth="3"
    className="animal" style={style}
    >
      <title>Tiger</title>
      <defs>
        <path id="tiger-ear" d="M33.7 18c-5.5-3.5-12.7-5.3-16.2-3.8-6 2.6-6.4 13.5-.2 22.1"/>
      </defs>
      <g id="tiger-l-ear" stroke="var(--tiger-outline)">
        <use href="#tiger-ear" fill="var(--tiger-light)"/>
        <use href="#tiger-ear" fill="none" stroke="var(--tiger-outline)"/>
        <path fill="var(--tiger-primary)" stroke="var(--tiger-outline)" d="m17.3 36.3c-3-4.2-4.5-9-4.5-13l2 0c2.4.2 6.3.7 7 4.4"/>
      </g>
      <use href="#tiger-l-ear" transform-origin="50px 0px 0px" transform="scale(-1,1)"/>
      <path id="tiger-jaw" fill="var(--tiger-light)" d="m88 54.1c0 0 0 12-1.5 15.7l-3.5-2.4c0 0-1.9 10.8-4.1 13.4-1.6.4-4.1-1.2-4.1-1.2 0 0-2.7 7.4-6.1 9.4-1.8.6-6.1-1.2-6.1-1.2-4.9 3.5-8.2 4.6-12.9 4.6-4.7 0-8.1-1.6-12.9-4.6 0 0-4.6 2-6.1 1.2-3.7-1.8-6.1-9.4-6.1-9.4 0 0-2.4 1.7-4.1 1.2-2.8-2.2-4.1-13.4-4.1-13.4l-3.5 2.4c-1.8-1.1-1.5-13-1.5-15.7 0-21.1 17.1-38.2 38.2-38.2 21.1 0 38.2 17.1 38.4 38.1z"/>
      <path id="tiger-face" fill="var(--tiger-primary)"
            d="M86.6 44.5c.2 3.1 1.5 8.8-1.2 10.5-1.8 6.6-5.7 13-12.3 15.5-13.9 5.3-7.5-7.6-23.3-7.6s-9.4 13-23.2 7.6C20 68 16 61.5 14.3 55c-2.7-1.7-1.4-7.4-1.2-10.5 1-16.2 17.6-28.6 36.7-28.6 19.2 0 35.8 12.4 36.8 28.6Z"/>
      <path id="tiger-eye" fill="var(--tiger-light)" d="m24 46c0 4 3.8 9.6 7.7 9.6 4 0 5.1-1.8 5.1-8.1 0-6.4-1.8-3.4-5-5.5-4.3-2.7-7.8-1-7.8 4z"/>
      <use href="#tiger-eye" transform-origin="50px 0px 0px" transform="scale(-1, 1)"/>
      <path id="tiger-nose" fill="var(--tiger-dark)"
            d="M47.5 62c-1.6-.3-3.4-.4-4.7.7-.9.7-1.2 2-.6 3a8.3 8.3 0 0 0 3.5 3.5c1.3.7 3 1.5 4.5 1.5 1.6 0 3.1-.8 4.5-1.5a8.3 8.3 0 0 0 3.5-3.4c.6-1 .3-2.4-.6-3.1-1.3-1.1-3.1-1-4.7-.6-.8.2-1.8.6-2.7.6-1 0-2-.4-2.7-.6z"/>
      <path id="tiger-mouth" fill="none" stroke="var(--tiger-dark)"
            d="m37 78c3 3.7 11.5-2 11.5-2l1.5-5.3 1.5 5.3s8.6 5.7 11.5 2"/>
      <g id="tiger-pupils" fill="var(--tiger-dark)">
        <circle id="circle904" cx="31.4" cy="50.1" r="2.5"/>
        <circle id="circle906" cx="68.5" cy="50.1" r="2.5"/>
      </g>
      <g stroke="var(--tiger-outline)">
        <path fill="none" d="M32 30.3a34 34 0 0 1 35.1 0"/>
        <path fill="none" d="M50 20.4v5"/>
        <path fill="none" d="M40.2 21.6a39.1 39.1 0 0 1 19.2 0"/>
        <g id="stripes-l">
          <path fill="none" d="M24 37.6c-4.8 1.3-7 8-8 12.7"/>
          <path fill="none" d="M24.5 55.3c-1.6 1.4-4.3 5-4.6 8.8"/>
          <path fill="none" d="M32.7 63.6a20.1 20.1 0 0 0 0 20.8"/>
        </g>
        <use id="stripes-r" href="#stripes-l" transform-origin="50px 0px 0px" transform="scale(-1, 1)"/>
      </g>
        <path id="tiger-outline" fill="none" stroke="var(--tiger-outline)" d="m88 54.1c0 0 0 12-1.5 15.7l-3.5-2.4c0 0-1.9 10.8-4.1 13.4-1.6.4-4.1-1.2-4.1-1.2 0 0-2.7 7.4-6.1 9.4-1.8.6-6.1-1.2-6.1-1.2-4.9 3.5-8.2 4.6-12.9 4.6-4.7 0-8.1-1.6-12.9-4.6 0 0-4.6 2-6.1 1.2-3.7-1.8-6.1-9.4-6.1-9.4 0 0-2.4 1.7-4.1 1.2-2.8-2.2-4.1-13.4-4.1-13.4l-3.5 2.4c-1.8-1.1-1.5-13-1.5-15.7 0-21.1 17.1-38.2 38.2-38.2 21.1 0 38.2 17.1 38.4 38.1z"/>
    </svg>
  )
}
